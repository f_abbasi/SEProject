var glass = 5;
var cardboard = 2;
var plastic = 3;
var aluminum = 3;
var other = 5;
var bike = 3;
var walk = 5;
var bus = 4;

let rcount = parseInt(localStorage.getItem("rCounter"));
let mcount = parseInt(localStorage.getItem("mCounter"));
let tcount = parseInt(localStorage.getItem("tCounter"))

var d = 0;
var tCounter = 0;
var mCounter = 0;
var rCounter = 0;
updateDisplay();

//fuction for when button submit button is clicked
document.getElementById("rsubmit").onclick = function(){

    //sets variable "userinput" as the element input 
    var ruserinput = parseInt(document.getElementById("rinput").value);
    localStorage.setItem("rlastInput", ruserinput); //stores variable "userinput" into "lastinput"
    
    //if anything other than 0 or higher is in userinput the counter will not change
    if (ruserinput >= 0 && ruserinput < 101)
    {
    rcount = ruserinput + rcount; 
    localStorage.setItem("rCounter", rcount);
    document.getElementById("recycle").innerHTML = parseInt(localStorage.getItem("rCounter"));
    updateDisplay();
  } else {
    localStorage.setItem("rCounter", rcount);
    }

  //this adds 1 to tree counter every 100 recycled items

  let i = parseInt(localStorage.getItem("rdivider"));
 // localStorage.setItem("rdivider", d);
  let x = rcount / 100;
    i++;

    if (parseInt(x) == i) {
        tcount++;
        localStorage.setItem("rdivider", i);
        localStorage.setItem("tCounter", tcount);
    }
    else {
        localStorage.setItem("tCounter", tcount);
    }
   
  //updates counter
  updateDisplay();
    
}   

//counter function
function updateDisplay(){
    //makes lui "counter" equal what is in localstorage "usercount"    
    document.getElementById("recycle").innerHTML = parseInt(localStorage.getItem("rCounter"));
    document.getElementById("miles").innerHTML = parseInt(localStorage.getItem("mCounter"));    
    document.getElementById("tree").innerHTML = parseInt(localStorage.getItem("tCounter"));
//    updateTree();
};

document.getElementById("recycle").innerHTML = parseInt(localStorage.getItem("rCounter"));
document.getElementById("miles").innerHTML = parseInt(localStorage.getItem("mCounter"));
document.getElementById("tree").innerHTML = parseInt(localStorage.getItem("tCounter"));
updateDisplay();  


//fuction for when button submit button is clicked
document.getElementById("msubmit").onclick = function(){

    //sets variable "userinput" as the element input 
    var muserinput = parseInt(document.getElementById("minput").value);
    localStorage.setItem("mlastInput", muserinput); //stores variable "userinput" into "lastinput"
    
    //if anything other than 0 or higher is in userinput the counter will not change
    if (muserinput >= 0 && muserinput < 101)
    {
    mcount = muserinput + mcount; 
    localStorage.setItem("mCounter", mcount);
    updateDisplay();
    } else {
    localStorage.setItem("mCounter", mcount);
    }


   //this adds 1 to tree counter every 100 recycled items
  let j = parseInt(localStorage.getItem("mdivider"));
  let y = mcount / 100;
    j++;

    if (parseInt(y) == j) {
        tcount++;
        localStorage.setItem("mdivider", j);
        localStorage.setItem("tCounter", tcount);
    }
    else {
        localStorage.setItem("tCounter", tcount);
    }

    //updates counter
    updateDisplay();  
}   

//adds the expanding button feature
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}