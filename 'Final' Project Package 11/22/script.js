//Software Engineering Project by Fariha Abbasi and Daisy CanoHernandez
//javascript file

//counter variables
var d = 0;
var tCounter = 0;
var mCounter = 0;
var rCounter = 0;
let rcount = parseInt(localStorage.getItem("rCounter"));
let mcount = parseInt(localStorage.getItem("mCounter"));
let tcount = parseInt(localStorage.getItem("tCounter"));

//checking to see if this is the user's first visit to the site
  //functions setCookie and getCookie were based off a code snippet by Peter B, posted 3/15/2018
  //the source snippet can be found at https://stackoverflow.com/questions/49304457/check-if-it-is-user-is-visiting-first-time-with-javascript
function setCookie(cookieName,cookieValue,days){var exdate=new Date();
  exdate.setDate(exdate.getDate() + days);var cookieValue=escape(cookieValue) + ((days==null) ? "" : "; expires="+exdate.toUTCString())
  ;document.cookie=cookieName + "=" + cookieValue;}

function getCookie(cookieName){var cookieValue = document.cookie;
  var start = cookieValue.indexOf(" " + cookieName + "=");
  if (start == -1){start = cookieValue.indexOf(cookieName + "=");}if (start == -1){cookieValue = null;}else{start = cookieValue.indexOf("=", start) + 1;
  var end = cookieValue.indexOf(";", start);
  if (end == -1){end = cookieValue.length;}cookieValue = unescape(cookieValue.substring(start,end));}return cookieValue;}
checkSession();

//the remaining code is original work by Fariha Abbasi and Daisy CanoHernandez
function checkSession(){
    var c = getCookie("visited");
    if (c === "yes") {
      return;
    } else {
    localStorage.setItem("rCounter", 0);
    localStorage.setItem("mCounter", 0);
    localStorage.setItem("tCounter", 0);
    rcount = 0;
    mcount = 0;
    tcount = 0;
    localStorage.setItem("rdivider", 0);
    localStorage.setItem("mdivider", 0);    
    }
   setCookie("visited", "yes", 365); // expire in 1 year; or use null to never expire
}

//Recycling
  //fuction for when button submit button is clicked
  document.getElementById("rsubmit").onclick = function(){

  //retrieving the input from the dropdown
  var e = document.getElementById("recycleDrop");
  var value = e.value;
  var text = e.options[e.selectedIndex].text;

  //if statement
  if (text == "glass" || text == "other") {
    var multiply = 5;
  } else if (text == "cardboard") {
    var multiply = 2;
  } else if (text == "plastic" || text == "aluminum") {
    var multiply = 3;
  }

  //sets variable "userinput" as the element input 
  var ruserinput = parseInt(document.getElementById("rinput").value);
  localStorage.setItem("rlastInput", ruserinput); //stores variable "userinput" into "lastinput"
  
  //if anything other than 0 or higher is in userinput the counter will not change
  if (ruserinput >= 0 && ruserinput < 101) {
    //multiply the amount entered by its number of points
    rcount = (ruserinput * multiply) + rcount; 
    localStorage.setItem("rCounter", rcount);
    document.getElementById("recycle").innerHTML = parseInt(localStorage.getItem("rCounter"));
    updateDisplay();
  } else {
    localStorage.setItem("rCounter", rcount);
    }

  //this adds 1 to tree counter every 100 recycled items
  let i = parseInt(localStorage.getItem("rdivider"));
  let x = rcount / 100;
    if (parseInt(x) == i) {
      i++;
      tcount++;
      localStorage.setItem("rdivider", i);
      localStorage.setItem("tCounter", tcount);
    }
    else {
      localStorage.setItem("tCounter", tcount);
    }

  //updates counter
  updateDisplay();
}   

//Transportation 
  //fuction for when button submit button is clicked
  document.getElementById("msubmit").onclick = function(){

  //retrieving input from dropdown menu
  var g = document.getElementById("transportDrop");
  var gvalue = g.value;
  var gtext = g.options[g.selectedIndex].text;

  //if statement
  if (gtext == "walk") {
    var gmultiply = 5;
  } else if (gtext == "bus") {
    var gmultiply = 4;
  } else if (gtext == "bike") {
    var gmultiply = 3;
  }

  //sets variable "userinput" as the element input 
  var muserinput = parseInt(document.getElementById("minput").value);
  localStorage.setItem("mlastInput", muserinput); //stores variable "userinput" into "lastinput"

  //if anything other than 0 or higher is in userinput the counter will not change
  if (muserinput >= 0 && muserinput < 101) {
    mcount = (muserinput * gmultiply) + mcount; 
    localStorage.setItem("mCounter", mcount);
    updateDisplay();
  } else {
    localStorage.setItem("mCounter", mcount);
    }

  //this adds 1 to tree counter every 100 recycled items
  let j = parseInt(localStorage.getItem("mdivider"));
  let y = mcount / 100;

  if (parseInt(y) == j) {
    j++;
    tcount++;
    localStorage.setItem("mdivider", j);
    localStorage.setItem("tCounter", tcount);
  } else {
      localStorage.setItem("tCounter", tcount);
  }

  //updates counter
  updateDisplay();  
}   

//counter function
function updateDisplay(){
  //makes lui "counter" equal what is in localstorage "usercount"    
  document.getElementById("recycle").innerHTML = parseInt(localStorage.getItem("rCounter"));
  document.getElementById("miles").innerHTML = parseInt(localStorage.getItem("mCounter"));    
  document.getElementById("tree").innerHTML = parseInt(localStorage.getItem("tCounter"));
};

document.getElementById("recycle").innerHTML = parseInt(localStorage.getItem("rCounter"));
document.getElementById("miles").innerHTML = parseInt(localStorage.getItem("mCounter"));
document.getElementById("tree").innerHTML = parseInt(localStorage.getItem("tCounter"));
updateDisplay();  

//variables for all collapsible items
var coll = document.getElementsByClassName("collapsible");
var h;
//adds the expanding button feature
for (h = 0; h < coll.length; h++) {
  coll[h].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}

